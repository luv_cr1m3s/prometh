"""Simple database
Main commands:
    EXIT -- to exit program from the main menu
    SHOW -- to show all notes in base
    SORT -- to sort all note by the key
    ADD  -- to add new item into base
    SAVE -- to save data into file
    LOAD -- to load data from the file
"""
import sys
import os
import json
# List of added items
DATASET = []
# You can change fields in EMPTY_FORM and everything should work
# For each key should be function 'checker' defined in INPUT_CHECKER
# INPUT_CHECKER defined below checker functions
EMPTY_FORM = {"name": '', "last name": '',
              "age": '', "experience": '', "salary": ''}

RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
BLUE = '\033[34m'
END = '\033[m'


def menu():
    """Menu with basic options"""
    # TO DO: add more options like SAVE, LOAD, DELETE
    try:
        print(YELLOW + "Use EXIT for exit from the program.\n" +
                       "Use SHOW to show entered data.\n" +
                       "Use SORT to sort present data.\n" +
                       "Use ADD to add new item into base.\n" +
                       "Use SAVE to save entered data in default file.\n" +
                       "Use LOAD to load data from thefile." + END)
        user_input = input(GREEN + "-->:" + END)
        if user_input == "EXIT":
            print("Bye")
            sys.exit(0)
        elif user_input == "SHOW":
            if len(DATASET) == 0:
                print(RED + "Sorry no items in the base, nothing to print." + END)
            else:
                print_db(DATASET)
        elif user_input == "SORT":
            if len(DATASET) == 0:
                print(RED + "Sorry nothing to sort." + END)
            else:
                data_sort()
        elif user_input == 'ADD':
            data_input()
        elif user_input == 'SAVE':
            filename = input(GREEN + "Enter filename or press Enter for default: " + END)
            if filename == '':
                data_save()
            else:
                data_save(filename)
        elif user_input == 'LOAD':
            filename = input(GREEN + "Enter filename to be loaded: " + END)
            if filename == '':
                print(RED + "Filename cannot be empty." + END)
            else:
                data_load(filename)
        else:
            print(RED + f"Wrong command: {user_input}." + END)
            raise ValueError
    except ValueError as e:
        print(e, "Please try again")


def data_input():
    """Ask user for input, validate data and add to DATASET"""
    # TO DO: make empty set constant
    personal_data = EMPTY_FORM.copy()

    while True:
        try:
            print(GREEN + "Please enter folowing data:" + END)
            for key, value in personal_data.items():
                if value == '':
                    user_input = input(BLUE + f"{key.capitalize()}: " + END)
                    flag = True
                    if key == "experience":
                        flag = check_expirience(user_input, personal_data['age'])
                    else:
                        flag = validate_input(INPUT_CHECKER[key], user_input)
                    if flag:
                        personal_data[key] = user_input
                    else:
                        raise ValueError
        except ValueError as e:
            print(e, "Please try again")
        else:
            DATASET.append(personal_data)
            print(BLUE + "New person added" + END)
            return 0


def validate_input(function, parameter):
    """I want to try make less rows"""
    return function(parameter)


def check_name(name):
    """Simple name check"""
    if name.isalpha():
        return True
    print(RED + "Use only letters for name." + END)
    return False


def check_last_name(last_name):
    """Simple last namecheck"""
    if last_name.isalpha():
        return True
    print(RED + "Use only letters for last name." + END)
    return False


def check_age(age):
    """age is numeric and less than 150"""
    if int(age) < 150 and age.isnumeric():
        return True
    print(RED + "Use only numbers for age less than 150." + END)
    return False


def check_expirience(expirience, age):
    """expirience is numeric and less than age"""
    if int(expirience) - int(age) < 0:
        return True
    print(RED + "Experience cannot be more than age." + END)
    return False


def check_salary(salary):
    """salary is positive numeric"""
    if int(salary) >= 0 and salary.isnumeric():
        return True
    print(RED + "Use only numbers for salary bigger or equal 0." + END)
    return False


# I don't know how to make expirience checker same as other functions
# I can't place it before functions
INPUT_CHECKER = {"name": check_name, "last name": check_last_name,
                 "age": check_age, "salary": check_salary}


def print_db(data):
    """Imagine correctly formated data:)"""
    # Can't use values from dict.items because some fields int->not iterable
    len_dict = EMPTY_FORM.copy()
    for key in len_dict.keys():
        len_dict[key] = len(key)

    # using same empty dictionary to form database and fields length
    for item in data:
        for len_k in len_dict.keys():
            if len(item[len_k]) > len_dict[len_k]:
                len_dict[len_k] = len(item[len_k])

    title_list = []
    for key in len_dict.keys():
        title_list.append("{:>{val}}".format(key.capitalize(), val=len_dict[key]))
    title_str = "# " + " # ".join(title_list) + " #"
    hash_len = len(title_str)

    print(BLUE + "#"*hash_len + END)
    print(BLUE + title_str + END)
    print(BLUE + "#"*hash_len + END)
    for item in data:
        for key in item.keys():
            print("# {:>{val}} ".format(item[key], val=len_dict[key]), end='')
        print("#")

    print(BLUE + "#"*hash_len + END)


def data_sort():
    """Sorts DATASET by key given by user"""
    keys = DATASET[0].keys()
    while True:
        key = input("Select the key:" + BLUE + f" {' | '.join(keys)}->:" + END)
        if key in keys:
            print_db(sorted(DATASET, key=lambda i: i[key]))
            return 0
        print(RED + "Wrong key, try again" + END)


def data_save(filename="default_base.txt"):
    """Saves DATASET into selected file."""
    if len(DATASET) == 0:
        print(YELLOW + "DATASET is empty, nothing to save." + END)
    else:
        with open(filename, "w") as file:
            for item in DATASET:
                json.dump(item, file)
                file.write("\n")

        print(GREEN + f"Saved data into {filename}." + END)


def data_load(filename="default_base.txt"):
    """Loads data from chosen file into DATASET"""
    global DATASET
    DATASET = []
    try:
        with open(filename, "r") as file:
            for line in file.readlines():
                DATASET.append(json.loads(line))
    except (OSError, IOError) as e:
        print(e, f"File {filename} does not exist")


def main():
    """Starting function"""
    print("Loading default base")
    if os.path.exists("default_base.txt"):
        print("Loading data from existing base.")
        data_load()
    else:
        print("Nothing was found." +
              " Creating empty base file.")
        open("default_base.txt", "w").close()
    while True:
        menu()


if __name__ == "__main__":
    main()
